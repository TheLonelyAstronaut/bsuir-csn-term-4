#include <dos.h>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <io.h>
#define COMMAND_NUMBER 6

void interrupt newInt9(...);					// Функция обработки прерывания
void interrupt (*oldInt9)(...); 			// Указатель на обработчик прерывания						

int isResend = 1; 										// Флаг ошибки / необходимости повторной передачи данных
int count = 0;	  										// Счетчик повторной передачи
int attribute = 0x03;
unsigned char command[20];
int pos = 0;

unsigned char commands[COMMAND_NUMBER][20] = {
	{0x2a, 0x04, 0x2e, 0x23, 0x20, 0x12, 0x26, 0x1e, 0x15, 0x04},   // #chdelay3
	{0x2a, 0x04, 0x2e, 0x23, 0x20, 0x12, 0x26, 0x1e, 0x15, 0x03},	  // #chdelay2
	{0x2a, 0x04, 0x2e, 0x23, 0x20, 0x12, 0x26, 0x1e, 0x15, 0x02},		// #chdelay1
	{0x2a, 0x04, 0x2e, 0x23, 0x20, 0x12, 0x26, 0x1e, 0x15, 0x0b},		// #chdelay0
	{0x2a, 0x04, 0x2e, 0x23, 0x1F, 0x19, 0x12, 0x12, 0x20, 0x02},		// #chspeed1
	{0x2a, 0x04, 0x2e, 0x23, 0x1F, 0x19, 0x12, 0x12, 0x20, 0x0b},		// #chspeed0
};

struct VIDEO
{
	unsigned char symbol;
	unsigned char attribute;
};

void _print_(unsigned char value, int offset = 0)
{
	VIDEO far *screen = (VIDEO far *)MK_FP(0xB800, 0); // get pointer to videobuff
	char temp = 0;

	screen += offset;

	if (count - offset >= 80)
		count = 0;

	if (!count)
	{
		for (int i = 0; i < 80; ++i)
		{
			screen->symbol = ' ';
			screen->attribute = attribute;
			++screen;
		}

		screen = (VIDEO far *)MK_FP(0xB800, 0);
	}
	else
	{
		screen += count;
	}

	int _value = value;
	char *key = new char[10];
	memset(key, 0, 10);
	itoa(_value, key, 16);

	for (int i = 0; i < strlen(key); i++)
	{
		screen->symbol = key[i];
		screen->attribute = attribute;
		++screen;
		++count;
	}

	screen->symbol = ' ';
	screen->attribute = attribute;
	++count;
}

void printCommand()
{
	VIDEO far *screen = (VIDEO far *)MK_FP(0xB800, 0);
	screen += 80;

	for(int i=0; i<pos; ++i){
		
		char *key = new char[10];
		memset(key, 0, 10);
		itoa(command[i], key, 16);

		for(int j=0; j<strlen(key); ++j){
			screen->symbol = key[j];
			screen->attribute = attribute;
			++screen;
		} 

		screen->symbol = ' ';
		screen->attribute = attribute;
		++screen;
	}
}

void autoDelay(unsigned char mask)
{
	isResend = 1;
	while (isResend)
	{
		while ((inp(0x64) & 0x02) != 0x00); // Ожидаем освобождения входного буфера клавиатуры
		outp(0x60, 0xF3);

		if(inp(0x60) == 0xFE) isResend = 1;
		else isResend = 0;
	}

	isResend = 1;
	while (isResend)
	{
		while ((inp(0x64) & 0x02) != 0x00);
		outp(0x60, mask);

		if(inp(0x60) == 0xFE) isResend = 1;
		else isResend = 0;
	}
}

void indentifyCommand()
{
	int match = 0;
	printCommand();

	for(int i = 0; i < COMMAND_NUMBER; ++i){
		match = 1;

		for(int j = 0; j < pos; ++j){
			if(command[j] != commands[i][j]){
				match = 0;
				break;
			}
		}

		if(match && pos == strlen((char*)commands[i])){
			switch (i){
			case 0:
				memset(command, 0, 20);
				pos = 20;
				printCommand();
				pos = 0;
				autoDelay(0x7F);					// 0111 1111
				break;
			case 1:
				memset(command, 0, 20);
				pos = 20;
				printCommand();
				pos = 0;
				autoDelay(0x3F);					// 0011 1111
				break;
			case 2:
				memset(command, 0, 20);
				pos = 20;
				printCommand();
				pos = 0;
				autoDelay(0x5F);					// 0101 1111
				break;
			case 3:
				memset(command, 0, 20);
				pos = 20;
				printCommand();
				pos = 0;
				autoDelay(0x1F);					// 0001 1111
				break;
			case 4:
				memset(command, 0, 20);
				pos = 20;
				printCommand();
				pos = 0;
				autoDelay(0x60);					// 0110 0000
				break;
			case 5:
				memset(command, 0, 20);
				pos = 20;
				printCommand();
				pos = 0;
				autoDelay(0x7F);					// 0111 1111
				break;
			}
		}
	}
}

void createCommand(unsigned char value)
{
	for(int i=0; i<pos; ++i){
		if(command[i] == value - 128){
			return;
		}
	}

	if(value == 0x2a){
		memset(command, 0, 20);
		pos = 0;
		command[pos++] = 0x2a;
	}

	if(value == 0x04 && pos == 1){
		command[pos++] = 0x04;
	}else if(pos >= 2 && pos < 20){
		command[pos++] = value;
	}else if(pos >= 20){
		memset(command, 0, 20);
		pos = 0;
	}
}

void interrupt newInt9(...)
{
	unsigned char value = 0;
	oldInt9();

	value = inp(0x60); // Получаем значение из порта 60h
	if (value == 0x01) exit(0); // Устанавливаем флаг выхода, если нажата Esc
	createCommand(value);
	indentifyCommand();
	//count = 0;
	_print_(value, 0);

	outp(0x20, 0x20); // Сброс контроллера прерываний
}

void init()
{
	_disable();
	oldInt9 = getvect(9); // Сохраняем указатель на старый обработчик
	setvect(9, newInt9);  // Меняем его на новый
	_enable();
}

int main()
{
	unsigned far *fp;
	init();

	FP_SEG(fp) = _psp;
	FP_OFF(fp) = 0x2c;
	_dos_freemem(*fp);
	_dos_keep(0, (_DS - _CS) + (_SP / 16) + 1);

	return 0;
}