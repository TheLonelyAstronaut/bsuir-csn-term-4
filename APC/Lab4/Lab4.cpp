#include <stdio.h>
#include <conio.h>
#include <dos.h>
#include <stdlib.h>

#define COUNT 8
#define DELAY 10
#define TIME_RUN 65536

void SoundGeneration();
void DivisionCoefficientComputation();
void StatusWord();
void RandomNumber();

void main() 
{
	clrscr();
	char choice;
	do
	{
		printf("\n\n1. Generate sound.\n2. Division coefficient.\n3. Status word.\n4. Random number.\n0. Exit.\n");
		rewind(stdin);
		printf("\nYour choise: ");
		scanf("%c", &choice);

		switch (choice) 
		{
		case '1': SoundGeneration(); break;
		case '2': DivisionCoefficientComputation(); break;
		case '3': StatusWord(); break;
		case '4': RandomNumber(); break;
		}

		system("pause");
		clrscr();
	} while (choice != '0');
}

void SoundGeneration() {
	int frequency[COUNT] = { 329, 246, 246, 261, 293, 329, 392, 349};
	int durability[COUNT] = {200, 400, 400, 400, 400, 400, 400, 400};
	int delayCounter[COUNT] = { 10, 10, 10, 10, 10, 10, 10, 10};
	long unsigned base = 1193180;
	int frequencyCounter;
	int divisionCoefficient;

	for (frequencyCounter = 0; frequencyCounter < COUNT; frequencyCounter++)
	{
		outp(0x43, 0xB6); //2 channel,read/write low and high byte,set up mode to 011(rectangle pulses)
                          // switch 1 and 0 on half-period 
		divisionCoefficient = base / frequency[frequencyCounter];
		outp(0x42, divisionCoefficient % 256);
		divisionCoefficient /= 256;
		outp(0x42, divisionCoefficient);	

		outp(0x61, inp(0x61) | 3);			
		delay(durability[frequencyCounter]);
		outp(0x61, inp(0x61) & 0xFC);			
		delay(delayCounter[frequencyCounter]);
	}
}

void DivisionCoefficientComputation() 
{
	unsigned long j;
	int Channel;
	int ports[] = { 0x40, 0x41, 0x42 };
	int controlWords[] = { 0, 64, 128 };
					  					//00 00 0000
                      					//01 00 0000
                      					//10 00 0000
	unsigned divisionCoefficientLow, divisionCoefficientHigh, divisionCoefficient, max;

	printf("\n\nCoefficient of division: \n");

	for (Channel = 0; Channel < 3; Channel++) 
	{
		divisionCoefficient = 0;
		max = 0;

		if (Channel == 2)
		{
			outp(0x61, inp(0x61) | 3);		//turn ON
		}

		for (j = 0; j < TIME_RUN; j++)
		{
			outp(0x43, controlWords[Channel]);
			divisionCoefficientLow = inp(ports[Channel]);
			divisionCoefficientHigh = inp(ports[Channel]);
			divisionCoefficient = divisionCoefficientHigh * 256 + divisionCoefficientLow;

			if (divisionCoefficient > max)
			{
				max = divisionCoefficient;
			}
		}
		if (Channel == 2) 
		{
			outp(0x61, inp(0x61) & 0xFC);	//turn OFF
		}
		printf("\nChannel %d: %4X\n", Channel, max);
	}
}

void StatusWord()
{
	printf("\n\nStatus word: \n");
	char stateWord[8];
	int Channel, state;
	int ports[] = { 0x40, 0x41, 0x42 };
	int controlWords[] = { 226, 228, 232 };		//command words,
												//acording to the rules
												//1 and 2 bit is 11 - command to read data
					  							//  11 1 0 001 0     	  // 001 - channel number
                      							//  11 1 0 010 0       // 4 bit is state of channel(if this bit 0)
                      							//  11 1 0 100 0       // 5 bit is value of counter(if this bit 0)

	for (Channel = 0; Channel < 3; Channel++)
	{
		outp(0x43, controlWords[Channel]);
		state = inp(ports[Channel]);

		for (int i = 7; i >= 0; i--) 
		{
			stateWord[i] = state % 2 + '0';
			state /= 2;

		}
		printf("\nChannel %d: ", Channel);
		for (i = 0; i < 8; i++) 
		{
			printf("%c", stateWord[i]);
		}
		printf("\n");
	}
}

void RandomNumber()
{
	char choice;
	unsigned int limit = TIME_RUN - 1, numberLow, numberHigh, number;

	do
	{
		printf("\n\n1. Set a limit.\n2. Get a number.\n0. Exit\n");
		fflush(stdin);
		printf("\nYour choise: ");
		scanf("%s", &choice);

		switch (choice)
		{
		case '1':
		{
			do
			{
				printf("\nEnter a limit in range [1...65535].\n");
				fflush(stdin);
			} while (!scanf("%d", &limit) || limit < 1 || limit > 65535);

			outp(0x43, 0xB4); //2 channel,read/write low and high byte,set up mode to 011(rectangle pulses)
                         	  // switch 1 and 0 on half-period 
			outp(0x42, limit % 256);
			limit /= 256;
			outp(0x42, limit);
			outp(0x61, inp(0x61) | 1);
			break;
		}
		case '2':
		{
			outp(0x43, 128);
			numberLow = inp(0x42);	
			numberHigh = inp(0x42);
			number = numberHigh * 256 + numberLow;

			printf("\nRandom number: %u\n", number);

			break;
		}
		}
		system("pause");
		clrscr();
	} while (choice != '0');
	outp(0x61, inp(0x61) & 0xFC);
}