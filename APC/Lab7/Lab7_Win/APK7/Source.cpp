#define _SCL_SECURE_NO_WARNINGS
#include <cstdio>
#include <windows.h>
#include <conio.h>
#include <ctime>
#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
#include <algorithm>
using namespace std;

void COM1(char* path);

int main(int argc, char* argv[])
{
	COM1(argv[0]);
}

void COM1(char* path)
{
	string name = "COM1";

	HANDLE handler;

	string message;

	handler = CreateFileA(
		name.c_str(),														// ��� ������������ �����.
		GENERIC_READ | GENERIC_WRITE,										// ��� ������� � �����.
		0,																	// ��������� ����������� �������.
		NULL,																// �������� ������ �����.
		OPEN_EXISTING,														// ����� ������������.
		FILE_ATTRIBUTE_NORMAL,												// ����������� ����� ������.
		NULL																// ��������� ����� �������.
	);

	SetupComm(handler, 1500, 1500);											// �������������� ���������������� ��������� ��� ��������� ���������� (����������, ����� �����-������)

	DCB COM_DCB;															// ���������, ��������������� �������� ��������� ����������������� �����. 

	memset(&COM_DCB, 0, sizeof(COM_DCB));										// ��������� ������ ��� ���������.
	COM_DCB.DCBlength = sizeof(DCB);											// ������ �����, � ������, ���������.
	GetCommState(handler, &COM_DCB);											// ��������� ������ � ������� ���������� ����������� �������� ��� ���������� ����������.

	COM_DCB.BaudRate = DWORD(115200);											// �������� �������� ������.
	COM_DCB.ByteSize = 8;													// ���������� ����� �������������� ��� � ������������ � ����������� ������.
	COM_DCB.Parity = NOPARITY;												// ���������� ����� ����� �������� �������� (��� ��������� �����������).
	COM_DCB.StopBits = ONESTOPBIT;											// ������ ���������� �������� ��� (���� ���). 


	if (!SetCommState(handler, &COM_DCB))
	{
		CloseHandle(handler);
		handler = INVALID_HANDLE_VALUE;
		return;
	}

	vector<string> commands;
	int cont = 0;

	while (true)
	{
		DWORD NumberOfBytesWritten;

		cin.clear();
		DWORD size = 0;
		DWORD readbyte = 0;

		ReadFile(handler, &size, sizeof(size), NULL, NULL);

		if (size == 0)
			continue;

		string str = "";

		DWORD needToRead = size; DWORD dwBytesRead = 0;

		while (needToRead)
		{
			char* buffer = new char[needToRead];
			memset(buffer, 0, needToRead);

			ReadFile(handler, buffer, needToRead, &dwBytesRead, NULL);

			if (dwBytesRead) {
				needToRead -= dwBytesRead;
				str.append(buffer, dwBytesRead);
			}
			else {
				Sleep(5);
			}
		}

		commands.push_back(str);

		if (commands.size() != 3)
			continue;

		cout << endl;

		for (int i = 0; i < 3; i++)
		{
			cout << i + 1 << "." << commands[i] << endl;
		}

		int answer;
		rewind(stdin);
		scanf_s("%d", &answer);

		WriteFile(handler, &answer, sizeof(answer), &NumberOfBytesWritten, NULL);

		if (NumberOfBytesWritten)
		{
			commands.clear();
			continue;
		}

	}

	system("pause");
	return;
}