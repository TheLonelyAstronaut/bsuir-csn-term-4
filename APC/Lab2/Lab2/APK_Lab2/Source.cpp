#define columnsNumber 4
#define rowsNumber 4
#define halfCycle 2
#define iterationCount 1000

#include <iostream>
#include <time.h>
#include <Windows.h>

int firstStartMatrix[rowsNumber][columnsNumber] = { 0 }, secondStartMatrix[rowsNumber][columnsNumber] = { 0 };
bool wasInitialized = false;

void copyToStartState(int firstMatrix[rowsNumber][columnsNumber], int secondMatrix[rowsNumber][columnsNumber])
{
	for (int i = 0; i < rowsNumber; ++i)
	{

		for (int j = 0; j < columnsNumber; ++j)
		{
			firstMatrix[i][j] = firstStartMatrix[i][j];
			secondMatrix[i][j] = secondStartMatrix[i][j];
		}
	}
}

void initializeMatrix(int firstMatrix[rowsNumber][columnsNumber], int secondMatrix[rowsNumber][columnsNumber])
{
	if (wasInitialized)
	{
		copyToStartState(firstMatrix, secondMatrix);
		return;
	}

	for (int i = 0; i < rowsNumber; ++i)
	{

		for (int j = 0; j < columnsNumber; ++j)
		{
			firstStartMatrix[i][j] = rand() % 10000;
			secondStartMatrix[i][j] = rand() % 1000;
		}
	}

	copyToStartState(firstMatrix, secondMatrix);
	wasInitialized = true;
}

void swapMatrix(int firstMatrix[rowsNumber][columnsNumber], int secondMatrix[rowsNumber][columnsNumber])
{
	for (int i = 0; i < rowsNumber; ++i)
	{
		for (int j = 0; j < columnsNumber; ++j)
		{
			int buffer = firstMatrix[i][j];
			firstMatrix[i][j] = secondMatrix[i][j] + 4;
			secondMatrix[i][j] = buffer + 4;
		}
	}
}

void printMatrix(int matrix[rowsNumber][columnsNumber])
{
	std::cout << std::endl;

	for (int i = 0; i < rowsNumber; ++i)
	{
		for (int j = 0; j < columnsNumber; ++j)
		{
			std::cout << matrix[i][j] << "\t";
		}

		std::cout << std::endl;
	}

	std::cout << std::endl;
}

int main()
{
	int firstMatrix[rowsNumber][columnsNumber] = { 0 }, secondMatrix[rowsNumber][columnsNumber] = { 0 }, four = 4;
	LARGE_INTEGER start, finish, frequency;
	
	initializeMatrix(firstMatrix, secondMatrix);

	std::cout << "Start values: " << std::endl;

	printMatrix(firstMatrix);
	printMatrix(secondMatrix);

	//Cpp block

	std::cout << "Swapping..." << std::endl;
	
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&start);

	for(int i=0; i<iterationCount; ++i) swapMatrix(firstMatrix, secondMatrix);

	QueryPerformanceCounter(&finish);

	std::cout << "\nC execution time: " << (finish.QuadPart - start.QuadPart) * 1000.0f / frequency.QuadPart << "\nResult:" << std::endl;
	printMatrix(firstMatrix);
	printMatrix(secondMatrix);

	//return to previous values
	initializeMatrix(firstMatrix, secondMatrix);

	//Pure ASM block

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&start);

	for (int i = 0; i < iterationCount; ++i)
	{
		_asm
		{
			xor esi, esi
			xor ecx, ecx
			mov ecx, columnsNumber*rowsNumber

			swapMatrixASM :
				mov eax, firstMatrix[esi]
				mov edx, secondMatrix[esi]
				add eax, four
				add edx, four
				mov firstMatrix[esi], edx
				mov secondMatrix[esi], eax
				add esi, 4
				loop swapMatrixASM
		}
	}

	QueryPerformanceCounter(&finish);

	std::cout << "\nPure ASM execution time: " << (finish.QuadPart - start.QuadPart) * 1000.0f / frequency.QuadPart << "\nResult:" << std::endl;
	printMatrix(firstMatrix);
	printMatrix(secondMatrix);

	//return to previous values
	initializeMatrix(firstMatrix, secondMatrix);

	//MMX block

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&start);

	for (int i = 0; i < iterationCount; ++i)
	{
		_asm
		{
			xor esi, esi
			xor ecx, ecx
			pxor MM0, MM0
			pxor MM0, MM1
			mov ecx, columnsNumber* halfCycle

			movd MM3, four
			movd MM1, four
			packssdw MM3, MM1

			MMXLoop :
				movq MM0, firstMatrix[esi]
				movq MM1, secondMatrix[esi]
				paddd MM0, MM3
				paddd MM1, MM3
				movq firstMatrix[esi], MM1
				movq secondMatrix[esi], MM0
				add esi, 8
				loop MMXLoop

			EMMS
		}
	}

	QueryPerformanceCounter(&finish);
	std::cout << "\nMMX execution time: " << (finish.QuadPart - start.QuadPart) * 1000.0f / frequency.QuadPart << "\nResult:" << std::endl;
	printMatrix(firstMatrix);
	printMatrix(secondMatrix);

	return 0;
}