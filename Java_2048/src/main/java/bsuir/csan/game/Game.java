package main.java.bsuir.csan.game;

import main.java.bsuir.csan.game.models.NumberBlock;

public class Game extends javafx.scene.canvas.Canvas {
    private Grid gameGrid;

    public Game() {
        super(330, 390);
        setFocused(true);
        resetGame();
    }

    public Game(double width, double height) {
        super(width, height);
        setFocused(true);
        resetGame();
    }

    public NumberBlock[] getCells() {
        return gameGrid.getCells();
    }

    public boolean canMove() {
        return gameGrid.canMove();
    }

    public boolean getWinState() {
        return  gameGrid.win;
    }

    public boolean getLoseState() {
        return  gameGrid.lose;
    }

    public void setLoseState(boolean state) {
        gameGrid.lose = state;  
    }

    public int getGameScore() {
        return gameGrid.score;
    }

    void resetGame() {
        gameGrid = new Grid(16);

        gameGrid.addCell();
        gameGrid.addCell();
    }

    public int getScore() {
        return gameGrid.getScore();
    }

    public void setProgress (NumberBlock[] field, int score) {
        gameGrid.score = score;
        gameGrid.setCells(field);
    }

    public void left() {
        boolean needAddCell = false;
        for(int i = 0; i < 4; i++) {
            NumberBlock[] line = gameGrid.getLine(i);
            NumberBlock[] merged = gameGrid.mergeLine(gameGrid.moveLine(line));
            gameGrid.setLine(i, merged);
            if( !needAddCell && !gameGrid.compare(line, merged)) {
                needAddCell = true;
            }
        }
        if(needAddCell) {
            gameGrid.addCell();
        }
    }

    public void right() {
        gameGrid.rotateAndSet(180);
        left();
        gameGrid.rotateAndSet(180);
    }

    public void up() {
        gameGrid.rotateAndSet(270);
        left();
        gameGrid.rotateAndSet(90);
    }

    public void down() {
        gameGrid.rotateAndSet(90);
        left();
        gameGrid.rotateAndSet(270);
    }
}