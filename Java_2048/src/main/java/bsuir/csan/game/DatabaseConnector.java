package main.java.bsuir.csan.game;

import java.sql.*;

public class DatabaseConnector {
    public static Connection connection;
    public static PreparedStatement statement; //PreparedStatement
    public static ResultSet resSet;

    public static void Connect() throws ClassNotFoundException, SQLException
    {
        connection = null;
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:2048.s3db");

        System.out.println("База Подключена!");
    }

    public static void WriteIntoTable(String table, String columns, String values) throws SQLException
    {
        String query = "INSERT INTO " + table + " (" + columns + ") VALUES (" + values +");";
        statement = connection.prepareStatement(query);
        statement.execute();

        System.out.println("Таблица заполнена");
    }

    public static void UpdateTable(String table, String data , String condition) throws SQLException
    {
        String query = "UPDATE " + table + " SET " + data + " WHERE " + condition + ";";
        statement = connection.prepareStatement(query);
        statement.execute();
    }

    public static void ReadDatabase(String query) throws ClassNotFoundException, SQLException
    {
        resSet = connection.prepareStatement("SELECT * FROM " + query).executeQuery();
    }

    public static void Execute(String query) throws ClassNotFoundException, SQLException
    {
        resSet = connection.prepareStatement(query).executeQuery();
    }

    public static void Disconnect() throws ClassNotFoundException, SQLException
    {
        connection.close();
        resSet.close();

        System.out.println("Соединения закрыты");
    }
}
