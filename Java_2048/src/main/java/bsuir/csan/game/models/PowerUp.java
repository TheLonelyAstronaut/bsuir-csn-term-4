package main.java.bsuir.csan.game.models;

enum PowerUpType {
    COLLAPSE,
    INCREASE_NUMBER,
    DECREASE_NUMBER
}

public class PowerUp extends GameObject {
    private PowerUpType type;

    public PowerUp(PowerUpType type) {
        this.type = type;
    }

    public PowerUpType getPowerUpType() {
        return this.type;
    }
}
