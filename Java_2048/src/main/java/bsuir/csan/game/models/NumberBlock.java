package main.java.bsuir.csan.game.models;
import javafx.scene.paint.Color;

import java.io.Serializable;

public class NumberBlock extends GameObject implements Serializable {
    int number;

    public NumberBlock() {
        this.number = 0;
    }

    public NumberBlock(int number) {
        this.number = number;
    }

    public boolean isEmpty() {
        return number == 0;
    }

    public Color getBackground() {
        switch (number) {
            case 2:		return Color.rgb(9,9,10, 1.0); //238 228 218 1.0      0xeee4da
            case 4: 	return Color.rgb(20,22,24, 1.0); //237, 224, 200, 1.0   0xede0c8
            case 8: 	return Color.rgb(32,34,37, 1.0); //242, 177, 121, 1.0   0xf2b179
            case 16: 	return Color.rgb(44,47,51, 1.0); //245, 149, 99, 1.0     0xf59563
            case 32: 	return Color.rgb(56,60,65, 1.0); //246, 124, 95, 1.0     0xf67c5f
            case 64:	return Color.rgb(68,72,78, 1.0 ); //246, 94, 59, 1.0      0xf65e3b
            case 128:	return Color.rgb(79,85,92, 1.0); //237, 207, 114, 1.0   0xedcf72
            case 256: 	return Color.rgb(91,98,106, 1.0); //237, 204, 97, 1.0     0xedcc61
            case 512: 	return Color.rgb(103,110,119, 1.0); //237, 200, 80, 1.0     0xedc850
            case 1024: 	return Color.rgb(114,123,133, 1.0); //237, 197, 63, 1.0     0xedc53f
            case 2048: 	return Color.rgb(127,136,145, 1.0); //237, 194, 46, 1.0     0xedc22e
        }
        return Color.rgb(35, 39, 42, 1.0); //0xcdc1b4
    }

    public Color getForeground() {
        Color foreground = Color.rgb(35, 39, 42, 1.0); //0x776e65
        return foreground;
    }

    public int getNumber() {
        return this.number;
    }

    public void setNumber(int num) {
        this.number = num;
    }
}
