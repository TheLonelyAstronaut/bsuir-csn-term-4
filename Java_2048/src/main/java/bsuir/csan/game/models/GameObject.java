package main.java.bsuir.csan.game.models;
import javafx.scene.Node;

public class GameObject extends Node {
    private int x,y;
    public static final int MAX_FIELD_SIZE = 4;

    public GameObject(){
        this.x = (int)Math.random() % MAX_FIELD_SIZE;
        this.y = (int)Math.random() % MAX_FIELD_SIZE;
    }

    public GameObject(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
