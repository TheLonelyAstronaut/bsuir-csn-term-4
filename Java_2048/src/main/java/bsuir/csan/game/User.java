package main.java.bsuir.csan.game;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.java.bsuir.csan.game.models.NumberBlock;

import java.sql.SQLException;
import java.util.Arrays;

public class User {
    private SimpleStringProperty username;
    private SimpleStringProperty password;
    private SimpleIntegerProperty maxScore;

    ////////////
    public String getUsername() {
        return username.get();
    }

    public void setUsername(String _username) {
        username.set(_username);
    }

    public String getPassword() {
        return password.get();
    }

    public void setPassword(String _password) {
        password.set(_password);
    }

    public int getMaxScore() {
        return maxScore.get();
    }

    public void setMaxScore(int _score) {
        maxScore.set(_score);
    }
    ///////////


    public User(String _username, String _password){
        username = new SimpleStringProperty();
        password = new SimpleStringProperty();
        maxScore = new SimpleIntegerProperty();

        username.set(_username);
        password.set(_password);
        maxScore.set(0);
    }

    public void readMaxScoreFromDatabase(){
        try{
            DatabaseConnector.ReadDatabase("scores WHERE login='" + username + "';");
            if(DatabaseConnector.resSet.next()){
                maxScore.set(DatabaseConnector.resSet.getInt("score"));
            }
        }catch (Exception ex){
            maxScore.set(0);
        }
    }

    public String getName(){
        return username.get();
    }

    static boolean isRegistered(String _username){
        try {
            DatabaseConnector.ReadDatabase("accounts WHERE login='" + _username + "';");

            if(DatabaseConnector.resSet.next()){
                return true;
            }
        }catch (Exception ex){

        }

        return false;
    }

    public static User registerUser(String _username, String _password){
        User user = new User("", "");

        if(isRegistered(_username)){
            user.maxScore.set(0);
            return user;
        }

        if(loginUser(_username, _password).username.get() == ""){
            try{
                DatabaseConnector.WriteIntoTable("'accounts'", "'login', 'password'", "'" + _username +"', '" + _password + "'");
                DatabaseConnector.WriteIntoTable("'scores'", "'login', 'score'", "'" + _username + "', 0");
                user.password.set(_password);
                user.username.set(_username);
                user.maxScore.set(0);
            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }

        return user;
    }

    public static User loginUser(String _username, String _password) {
        User user = new User("", "");

        try {
            DatabaseConnector.ReadDatabase("accounts WHERE login='" + _username + "' AND password='" + _password + "';");

            if(DatabaseConnector.resSet.next()) {
                user = new User(_username, _password);
            }

        }catch (Exception ex){
            user.maxScore.set(0);
        }

        return user;
    }

    public static ObservableList<User> getScores() {
        ObservableList<User> list = FXCollections.observableArrayList();

        try {
            DatabaseConnector.Execute("SELECT * from scores");

            while (DatabaseConnector.resSet.next()) {
                int score = DatabaseConnector.resSet.getInt("score");
                String login = DatabaseConnector.resSet.getString("login");

                User user = new User(login, "");
                user.maxScore.set(score);

                list.add(0, user);
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        return list;
    }

    public void setScore(int score){
        if(username.get() != "" && score > maxScore.get()) {
            try{
                DatabaseConnector.UpdateTable("'scores'", "score=" + score, "login='" + username.get() + "'");
                maxScore.set(score);
            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    public NumberBlock[] getUserProgress() {
        NumberBlock[] gameField = new NumberBlock[16];

        try {
            DatabaseConnector.ReadDatabase("accounts WHERE login='" + this.username.get() + "';");

            String[] fromDatabase = DatabaseConnector.resSet.getString("field").replaceAll(" ", "")
                                    .replaceAll("\\[", "").replaceAll("\\]", "").split(",");

            for(int i=0; i<16; ++i) {
                gameField[i] = new NumberBlock(Integer.parseInt(fromDatabase[i]));
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return  gameField;
    }

    public int getCurrentScore() {
        int currentScore = 0;

        try {
            DatabaseConnector.ReadDatabase("accounts WHERE login='" + this.username.get() + "';");

            if(DatabaseConnector.resSet.next())
            {
                currentScore = DatabaseConnector.resSet.getInt("currentScore");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return currentScore;
    }

    public void setUserProgress(NumberBlock[] gameField) {
        int[] toDatabase = new int[16];

        for(int i=0; i<16; ++i) {
            toDatabase[i] = gameField[i].getNumber();
        }

        String serialized = Arrays.toString(toDatabase);

        try {
            DatabaseConnector.UpdateTable("'accounts'", "field='" + serialized + "'", "login='"+this.username.get()+"'");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setCurrentScore(int score) {
        try {
            DatabaseConnector.UpdateTable("'accounts'", "currentScore=" + score, "login='"+this.username.get()+"'");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}


//https://api.unsplash.com/photos/random?client_id=SRwfZHEcgj3Bm8Pqnv-GSQuSkTsttlCUO-EXpu3tkSE&count=10



