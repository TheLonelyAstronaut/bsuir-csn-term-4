package main.java.bsuir.csan.game;

import main.java.bsuir.csan.game.models.NumberBlock;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Grid{
    private NumberBlock[] cells;
    boolean win = false;
    boolean lose = false;
    int score = 0;

    public NumberBlock[] getCells() {
        return cells;
    }
    public void setCells(NumberBlock[] field) { cells = field; }

    public Grid(int cellsCount){
        score = 0;
        win = false;
        lose = false;
        cells = new NumberBlock[4 * 4];

        for (int cell = 0; cell < cells.length; cell++) {
            cells[cell] = new NumberBlock();
        }
    }

    public void addCell() {
        List<NumberBlock> list = availableSpace();
        if(!availableSpace().isEmpty()) {
            int index = (int) (Math.random() * list.size()) % list.size();
            NumberBlock emptyCell = list.get(index);
            emptyCell.setNumber(Math.random() < 0.9 ? 2 : 4);
        }
    }

    private List<NumberBlock> availableSpace() {
        List<NumberBlock> list = new ArrayList<>(16);
        for(NumberBlock c : cells)
            if(c.isEmpty())
                list.add(c);
        return list;
    }
    private boolean isFull() {
        return availableSpace().size() == 0;
    }

    private NumberBlock cellAt(int x, int y) {
        return cells[x + y * 4];
    }

    protected boolean canMove() {
        if(!isFull()) return true;
        for(int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                NumberBlock cell = cellAt(x, y);
                if ((x < 3 && cell.getNumber() == cellAt(x + 1, y).getNumber()) ||
                        (y < 3) && cell.getNumber() == cellAt(x, y + 1).getNumber()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean compare(NumberBlock[] line1, NumberBlock[] line2) {
        if(line1 == line2) {
            return true;
        }
        if (line1.length != line2.length) {
            return false;
        }

        for(int i = 0; i < line1.length; i++) {
            if(line1[i].getNumber() != line2[i].getNumber()) {
                return false;
            }
        }
        return true;
    }

    private NumberBlock[] rotate(int angle) {
        NumberBlock[] tiles = new NumberBlock[4 * 4];
        int offsetX = 3;
        int offsetY = 3;
        if(angle == 90) {
            offsetY = 0;
        } else if(angle == 270) {
            offsetX = 0;
        }

        double rad = Math.toRadians(angle);
        int cos = (int) Math.cos(rad);
        int sin = (int) Math.sin(rad);
        for(int x = 0; x < 4; x++) {
            for(int y = 0; y < 4; y++) {
                int newX = (x*cos) - (y*sin) + offsetX;
                int newY = (x*sin) + (y*cos) + offsetY;
                tiles[(newX) + (newY) * 4] = cellAt(x, y);
            }
        }
        return tiles;
    }

    public NumberBlock[] moveLine(NumberBlock[] oldLine) {
        LinkedList<NumberBlock> list = new LinkedList<NumberBlock>();
        for(int i = 0; i < 4; i++) {
            if(!oldLine[i].isEmpty()){
                list.addLast(oldLine[i]);
            }
        }

        if(list.size() == 0) {
            return oldLine;
        } else {
            NumberBlock[] newLine = new NumberBlock[4];
            while (list.size() != 4) {
                list.add(new NumberBlock());
            }
            for(int j = 0; j < 4; j++) {
                newLine[j] = list.removeFirst();
            }
            return newLine;
        }
    }

    public NumberBlock[] mergeLine(NumberBlock[] oldLine) {
        LinkedList<NumberBlock> list = new LinkedList<NumberBlock>();
        for(int i = 0; i < 4 && !oldLine[i].isEmpty(); i++) {
            int num = oldLine[i].getNumber();
            if (i < 3 && oldLine[i].getNumber() == oldLine[i+1].getNumber()) {
                num *= 2;
                score += num;
                if ( num == 2048) {
                    win = true;
                }
                i++;
            }
            list.add(new NumberBlock(num));
        }

        if(list.size() == 0) {
            return oldLine;
        } else {
            while (list.size() != 4) {
                list.add(new NumberBlock());
            }
            return list.toArray(new NumberBlock[4]);
        }
    }

    public NumberBlock[] getLine(int index) {
        NumberBlock[] result = new NumberBlock[4];
        for(int i = 0; i < 4; i++) {
            result[i] = cellAt(i, index);
        }
        return result;
    }

    public int getScore() {
        return score;
    }

    public void setLine(int index, NumberBlock[] re) {
        System.arraycopy(re, 0, cells, index * 4, 4);
    }

    public void rotateAndSet(int value) {
        cells = rotate(value);
    }
}