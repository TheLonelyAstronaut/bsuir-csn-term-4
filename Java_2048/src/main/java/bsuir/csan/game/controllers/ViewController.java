package main.java.bsuir.csan.game.controllers;

import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.java.bsuir.csan.game.User;

public class ViewController {
    protected Stage currentStage;
    protected User user;
    protected VBox mainVBox;

    public ViewController(Stage _stage, User _user, VBox _mainVBox) {
        currentStage = _stage;
        user = _user;
        mainVBox = _mainVBox;
    }
}
