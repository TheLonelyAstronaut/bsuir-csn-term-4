package main.java.bsuir.csan.game.controllers;
import main.java.bsuir.csan.game.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class AppController extends VBox {
    private Stage currentStage;
    private User user;
    LoginController loginController;
    RegisterController registerController;
    MenuController menuController;
    GameController gameController;
    ResultsController resultsController;

    public AppController(Stage _stage){
        this.currentStage = _stage;

        this.setAlignment(Pos.CENTER);
        this.setBackground(new Background(new BackgroundFill(Color.web("#2c2f33"), CornerRadii.EMPTY, Insets.EMPTY)));

        loginController = new LoginController(currentStage, user, this);
        menuController = new MenuController(currentStage, user, this);
        registerController = new RegisterController(currentStage, user, this);
        gameController = new GameController(currentStage, user, this);
        resultsController = new ResultsController(currentStage, user, this);

        loginController.setNavigatingControllers(new LoginCallback() {
            @Override
            public void login(User _user) {
                navigateToMenu(_user);
                gameController.setNewUser(_user);
            }
        }, new NavigatingCallback() {
            @Override
            public void navigate() {
                registerController.draw();
            }
        });

        registerController.setNavigatingControllers(new LoginCallback() {
            @Override
            public void login(User _user) {
                navigateToMenu(_user);
                gameController.setNewUser(_user);
            }
        }, new NavigatingCallback() {
            @Override
            public void navigate() {
                loginController.draw();
            }
        });

        menuController.setNavigatingControllers(new NavigatingCallback() {
            @Override
            public void navigate() {
                gameController.draw();
            }
        }, new NavigatingCallback() {
            @Override
            public void navigate() {
                resultsController.draw();
            }
        }, new NavigatingCallback() {
            @Override
            public void navigate() {
                menuController.setNewUser(null);
                gameController.setNewUser(null);
                user = null;
                loginController.draw();
            }
        });

        gameController.setNavigatingControllers(new NavigatingCallback() {
            @Override
            public void navigate() {
                navigateToMenu(null);
            }
        });

        resultsController.setNavigatingControllers(new NavigatingCallback() {
            @Override
            public void navigate() {
                navigateToMenu(null);
            }
        });

        loginController.draw();
    }

    public void navigateToMenu (User _user) {
        if(user != null) {
            user = _user;
            user.readMaxScoreFromDatabase();

            menuController.setNewUser(user);
        }

        menuController.draw();
    }
}
