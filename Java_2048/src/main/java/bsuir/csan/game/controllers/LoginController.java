package main.java.bsuir.csan.game.controllers;

import com.dukescript.layouts.flexbox.FlexboxLayout;
import com.dukescript.layouts.jfxflexbox.FlexBoxPane;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.java.bsuir.csan.game.User;

import java.io.UnsupportedEncodingException;

public class LoginController extends ViewController {
    LoginCallback menu;
    NavigatingCallback register;

    public LoginController(Stage _stage, User _user, VBox _mainVBox) {
        super(_stage, _user, _mainVBox);
    }

    public void setNavigatingControllers(LoginCallback _menu, NavigatingCallback _register) {
        menu = _menu;
        register = _register;
    }

    public void draw() {
        mainVBox.getChildren().clear();
        FlexBoxPane flex = new FlexBoxPane();

        Text text = new Text();
        text.setText("Login");
        text.setFill(Paint.valueOf("#7289da"));
        text.setStyle("-fx-font-size: 3em;");
        text.setFont(Font.loadFont("file:resources/fonts/UniSansHeavy.otf", 120));

        CustomTextField loginField = new CustomTextField("Login");
        CustomTextField passwordField = new CustomTextField("Password");

        flex.setAlignItems(FlexboxLayout.ALIGN_ITEMS_CENTER);
        flex.setFlexDirection(FlexboxLayout.FLEX_DIRECTION_ROW);
        flex.setJustifyContent(FlexboxLayout.JUSTIFY_CONTENT_CENTER);

        CustomButton loginButton = new CustomButton("Login");
        FlexBoxPane.setGrow(loginButton, 1.0f);
        FlexBoxPane.setMargin(loginButton, new Insets(5));
        FlexBoxPane.setFlexBasisPercent(loginButton, 10f);

        loginButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                User tempUser = User.loginUser(loginField.getText(), passwordField.getText());

                try {
                    if(tempUser.getName().getBytes("US-ASCII").length != 0){
                        mainVBox.getChildren().clear();
                        menu.login(tempUser);

                    }else{
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error!");
                        alert.setHeaderText(null);
                        alert.setContentText("Incorrect data!");
                        alert.showAndWait();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

        CustomButton registerButton = new CustomButton("Sign Up");
        FlexBoxPane.setGrow(registerButton, 1.0f);
        FlexBoxPane.setMargin(registerButton, new Insets(5));
        FlexBoxPane.setFlexBasisPercent(registerButton, 10f);
        registerButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                register.navigate();
            }
        });

        flex.getChildren().add(loginButton);
        flex.getChildren().add(registerButton);
        mainVBox.getChildren().add(text);
        mainVBox.getChildren().addAll(loginField, passwordField);
        mainVBox.getChildren().add(flex);
        mainVBox.setSpacing(20);

        mainVBox.setMargin(flex, new Insets(0, 50, 50, 50));
        mainVBox.setMargin(loginField, new Insets(0, 50, 0, 50));
        mainVBox.setMargin(passwordField, new Insets(0, 50, 0, 50));
        mainVBox.setMargin(text, new Insets(50, 50, 0, 50));
    }
}
