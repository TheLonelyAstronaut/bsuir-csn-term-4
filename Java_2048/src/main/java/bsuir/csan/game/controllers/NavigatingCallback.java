package main.java.bsuir.csan.game.controllers;

public interface NavigatingCallback {
    void navigate();
}
