package main.java.bsuir.csan.game.controllers;

import javafx.animation.AnimationTimer;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import main.java.bsuir.csan.game.Game;
import main.java.bsuir.csan.game.User;
import main.java.bsuir.csan.game.models.NumberBlock;

public class GameController extends ViewController {
    NavigatingCallback toMenu;
    static double CELL_SIZE = 64;
    final int SCORE_OFFSET = 100;

    public GameController(Stage _stage, User _user, VBox _mainVBox) {
        super(_stage, _user, _mainVBox);
    }

    public void setNavigatingControllers(NavigatingCallback _toMenu) {
        toMenu = _toMenu;
    }

    public void setNewUser(User _user) {
        user = _user;
    }

    public void draw() {
        Game game = new Game();

        int currentScore = user.getCurrentScore();

        if(currentScore != 0) {
            game.setProgress(user.getUserProgress(), currentScore);
        }

        currentStage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                System.out.println("Here");

                if (event.getCode() == KeyCode.ESCAPE) {
                    if(!game.getLoseState() && !game.getWinState()) {
                        user.setCurrentScore(game.getScore());
                        user.setUserProgress(game.getCells());
                    }

                    toMenu.navigate();

                    currentStage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
                        @Override
                        public void handle(KeyEvent event) { }
                    });
                }

                if (!game.canMove() || (!game.getWinState() && !game.canMove())) {
                    game.setLoseState(true);
                    user.setScore(game.getScore());
                    user.setCurrentScore(0);
                }

                if (!game.getWinState() && !game.getLoseState()) {
                    switch (event.getCode()) {
                        case LEFT:
                            game.left();
                            break;
                        case RIGHT:
                            game.right();
                            break;
                        case DOWN:
                            game.down();
                            break;
                        case UP:
                            game.up();
                            break;
                    }
                }
                game.relocate(330, 390);
            }
        });

        GraphicsContext gc = game.getGraphicsContext2D();

        game.setWidth(currentStage.getWidth());
        game.setHeight(currentStage.getHeight() - SCORE_OFFSET);
        gc.setFill(Color.rgb(44, 47, 51, 1.0));
        gc.fillRect(0, 0, game.getWidth(), game.getHeight());

        if(game.getWidth() > game.getHeight()) {
            CELL_SIZE = game.getHeight() / 6;
        }else{
            CELL_SIZE = game.getWidth() / 6;
        }

        mainVBox.widthProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                game.setWidth(currentStage.getWidth());
                game.setHeight(currentStage.getHeight() - SCORE_OFFSET);
                gc.setFill(Color.rgb(44, 47, 51, 1.0));
                gc.fillRect(0, 0, game.getWidth(), game.getHeight());

                if(game.getWidth() > game.getHeight()) {
                    CELL_SIZE = game.getHeight() / 6;
                }else{
                    CELL_SIZE = game.getWidth() / 6;
                }
            }
        });

        mainVBox.heightProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                game.setWidth(currentStage.getWidth());
                game.setHeight(currentStage.getHeight() - SCORE_OFFSET);

                gc.setFill(Color.rgb(44, 47, 51, 1.0));
                gc.fillRect(0, 0, game.getWidth(), game.getHeight());

                if(game.getWidth() > game.getHeight()) {
                    CELL_SIZE = game.getHeight() / 6;
                }else{
                    CELL_SIZE = game.getWidth() / 6;
                }
            }
        });

        Text score = new Text("Score");
        score.setFont(Font.loadFont("file:resources/fonts/UniSansHeavy.otf", 38));
        score.setFill(Color.rgb(144,137,218,1));

        mainVBox.getChildren().clear();
        mainVBox.getChildren().add(game);
        mainVBox.getChildren().add(score);
        mainVBox.setMargin(score, new Insets(20, 20, 20, 20));

        new AnimationTimer() {
            @Override
            public void handle(long now) {
                for(int y = 0; y < 4; y++) {
                    for(int x = 0; x < 4; x++){
                        NumberBlock cell = game.getCells()[x + y * 4];
                        int value = cell.getNumber();
                        double xOffset = widthOffsetCoors(x);
                        double yOffset = heightOffsetCoors(y);

                        gc.setFill(cell.getBackground());
                        gc.fillRoundRect(xOffset, yOffset, CELL_SIZE, CELL_SIZE, 14, 14);
                        gc.setFill(cell.getForeground());

                        final int size = value < 100 ? 32 : value < 1000 ? 28 : 24;
                        gc.setFont(Font.loadFont("file:resources/fonts/UniSansHeavy.otf", size));
                        gc.setFill(Color.rgb(144, 137, 218, 1));
                        gc.setTextAlign(TextAlignment.CENTER);
                        gc.setTextBaseline(VPos.CENTER);


                        String s = String.valueOf(value);

                        if (value != 0)
                            gc.fillText(s, xOffset + CELL_SIZE / 2, yOffset + CELL_SIZE / 2 - 2);
                        gc.setFill(cell.getForeground());
                        if(game.getWinState() || game.getLoseState()) {
                            gc.setFill(Color.rgb(44, 47, 51, 1.0));
                            gc.fillRect(0, 0, game.getWidth(), game.getHeight());
                            gc.setFill(Color.rgb(144,137,218,1));
                            gc.setFont(Font.loadFont("file:resources/fonts/UniSansHeavy.otf", 38));


                            if(game.getWinState()){
                                gc.fillText("You win!", (game.getWidth() - "You win!".length() ), (game.getHeight() - 50) / 2);
                            }


                            if(game.getLoseState()) {
                                gc.fillText("Game over!", (game.getWidth() - "Game over!".length()) / 2, (game.getHeight() - 50) / 2);
                            }
                        }

                        score.setText("Score: " + game.getGameScore());
                    }
                }
            }
        }.start();
    }

    private double widthOffsetCoors(int arg) {
        double widthOffset = (mainVBox.getWidth() - 3*16 - 4*CELL_SIZE) / 2;
        return widthOffset + arg * (16 + CELL_SIZE) + 8;
    }

    private double heightOffsetCoors(int arg) {
        double heightOffset = (mainVBox.getHeight() - SCORE_OFFSET - 3*16 - 4*CELL_SIZE) / 2 ;
        return heightOffset + arg * (16 + CELL_SIZE) + 8;
    }
}
