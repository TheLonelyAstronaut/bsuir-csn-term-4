package main.java.bsuir.csan.game.controllers;

import com.dukescript.layouts.jfxflexbox.FlexBoxPane;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.java.bsuir.csan.game.User;

import java.io.UnsupportedEncodingException;

public class RegisterController extends ViewController {
    LoginCallback toMenu;
    NavigatingCallback toLogin;

    public RegisterController(Stage _stage, User _user, VBox _mainVBox) {
        super(_stage, _user, _mainVBox);
    }

    public void setNavigatingControllers(LoginCallback _toMenu, NavigatingCallback _toLogin) {
        toLogin = _toLogin;
        toMenu = _toMenu;
    }

    public void draw() {
        mainVBox.getChildren().clear();

        Text text = new Text();
        text.setText("Register");
        text.setFill(Paint.valueOf("#7289da"));
        text.setStyle("-fx-font-size: 3em;");
        text.setFont(Font.loadFont("file:resources/fonts/UniSansHeavy.otf", 120));

        CustomTextField loginField = new CustomTextField("Login");
        CustomTextField passwordField = new CustomTextField("Password");

        CustomButton registerButton = new CustomButton("Sign Up");
        FlexBoxPane.setGrow(registerButton, 1.0f);
        FlexBoxPane.setMargin(registerButton, new Insets(5));
        FlexBoxPane.setFlexBasisPercent(registerButton, 10f);
        registerButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                User tempUser = User.registerUser(loginField.getText(), passwordField.getText());

                try {
                    if(tempUser.getName().getBytes("US-ASCII").length != 0){
                        mainVBox.getChildren().clear();
                        toMenu.login(tempUser);

                        currentStage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
                            @Override
                            public void handle(KeyEvent event) { }
                        });
                    }else{
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error!");
                        alert.setHeaderText(null);
                        alert.setContentText("Check input data or try another login!");
                        alert.showAndWait();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

        currentStage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                System.out.println("Here");

                if (event.getCode() == KeyCode.ESCAPE) {
                    toLogin.navigate();

                    currentStage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
                        @Override
                        public void handle(KeyEvent event) { }
                    });
                }
            }
        });

        mainVBox.getChildren().addAll(text, loginField, passwordField, registerButton);
        mainVBox.setSpacing(20);
        mainVBox.setMargin(registerButton, new Insets(0, 50, 50, 50));
        mainVBox.setMargin(loginField, new Insets(0, 50, 0, 50));
        mainVBox.setMargin(passwordField, new Insets(0, 50, 0, 50));
        mainVBox.setMargin(text, new Insets(50, 50, 0, 50));
    }
}
