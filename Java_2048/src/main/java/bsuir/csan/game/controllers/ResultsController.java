package main.java.bsuir.csan.game.controllers;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.java.bsuir.csan.game.User;

public class ResultsController extends ViewController {
    NavigatingCallback toMenu;

    public ResultsController(Stage _stage, User _user, VBox _mainVBox) {
        super(_stage, _user, _mainVBox);
    }

    public void setNavigatingControllers(NavigatingCallback _toMenu) {
        toMenu = _toMenu;
    }

    public void draw() {
        mainVBox.getChildren().clear();

        TableView<User> users = new TableView<User>(User.getScores());
        users.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        TableColumn<User, String> nameColumn = new TableColumn<User, String>("Username");
        nameColumn.setCellValueFactory(new PropertyValueFactory<User, String>("username"));
        users.getColumns().add(nameColumn);

        TableColumn<User, String> scoreColumn = new TableColumn<User, String>("Score");
        scoreColumn.setCellValueFactory(new PropertyValueFactory<User, String>("maxScore"));
        scoreColumn.setSortType(TableColumn.SortType.DESCENDING);
        users.getColumns().add(scoreColumn);
        users.getSortOrder().add(scoreColumn);
        users.sort();

        users.setMaxWidth(mainVBox.getWidth() - 50);
        users.setMinWidth(mainVBox.getWidth() - 50);
        users.setMinHeight(mainVBox.getHeight() - 50);
        users.setMaxHeight(mainVBox.getHeight() - 50);

        currentStage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.ESCAPE) {
                    toMenu.navigate();

                    currentStage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
                        @Override
                        public void handle(KeyEvent event) { }
                    });
                }
            }
        });

        mainVBox.widthProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                users.setMaxWidth(mainVBox.getWidth() - 50);
                users.setMinWidth(mainVBox.getWidth() - 50);
                users.setMinHeight(mainVBox.getHeight() - 50);
                users.setMaxHeight(mainVBox.getHeight() - 50);

            }
        });

        mainVBox.heightProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                users.setMaxWidth(mainVBox.getWidth() - 50);
                users.setMinWidth(mainVBox.getWidth() - 50);
                users.setMinHeight(mainVBox.getHeight() - 50);
                users.setMaxHeight(mainVBox.getHeight() - 50);

            }
        });

        mainVBox.getChildren().add(users);
    }
}
