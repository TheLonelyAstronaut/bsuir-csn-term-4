package main.java.bsuir.csan.game.controllers;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.text.Font;

public class CustomButton extends Button {

    public CustomButton(String text){
        this.setText(text);
        this.setPrefHeight(50);
        this.setMinWidth(20);
        this.setStyle("-fx-background-color: #7289da; -fx-text-fill: #ffcccc;");
        this.setFont(Font.loadFont("file:resources/fonts/UniSansHeavy.otf", 14));
        this.setMaxWidth(Double.MAX_VALUE);
        this.setAlignment(Pos.CENTER);
    }
}
