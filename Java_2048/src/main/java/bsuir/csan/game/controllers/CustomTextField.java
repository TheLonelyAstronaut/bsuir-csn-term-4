package main.java.bsuir.csan.game.controllers;

import javafx.scene.control.TextField;
import javafx.scene.text.Font;

public class CustomTextField extends TextField {
    CustomTextField(String prompt) {
        super();
        this.setPromptText(prompt);
        this.setFont(Font.loadFont("file:resources/fonts/UniSansHeavy.otf", 14));
        this.setStyle("-fx-background-color: #23272a; -fx-text-fill: #ffffff;");
    }
}
