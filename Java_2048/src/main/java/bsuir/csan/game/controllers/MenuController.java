package main.java.bsuir.csan.game.controllers;

import com.dukescript.layouts.flexbox.FlexboxLayout;
import com.dukescript.layouts.jfxflexbox.FlexBoxPane;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.java.bsuir.csan.game.User;

import java.io.InputStream;

public class MenuController extends ViewController {
    NavigatingCallback toGame;
    NavigatingCallback toResults;
    NavigatingCallback exit;

    public MenuController(Stage _stage, User _user, VBox _mainVBox) {
        super(_stage, _user, _mainVBox);
    }

    public void setNewUser(User _user) {
        user = _user;
    }

    public void setNavigatingControllers(NavigatingCallback _toGame, NavigatingCallback _toResults, NavigatingCallback _exit) {
        toGame = _toGame;
        toResults = _toResults;
        exit = _exit;
    }

    public void draw() {
        mainVBox.getChildren().clear();
        FlexBoxPane flex = new FlexBoxPane();

        flex.setAlignItems(FlexboxLayout.ALIGN_ITEMS_CENTER);
        flex.setFlexDirection(FlexboxLayout.FLEX_DIRECTION_ROW);
        flex.setJustifyContent(FlexboxLayout.JUSTIFY_CONTENT_CENTER);;

        CustomButton button = new CustomButton("Play");
        FlexBoxPane.setGrow(button, 1.0f);
        FlexBoxPane.setMargin(button, new Insets(5));
        FlexBoxPane.setFlexBasisPercent(button, 10f);
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                toGame.navigate();
            }
        });
        flex.getChildren().add(button);

        button = new CustomButton("Results");
        FlexBoxPane.setGrow(button, 1.0f);
        FlexBoxPane.setMargin(button, new Insets(5));
        FlexBoxPane.setFlexBasisPercent(button, 10f);
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                toResults.navigate();
            }
        });
        flex.getChildren().add(button);

        button = new CustomButton("Exit");
        FlexBoxPane.setGrow(button, 1.0f);
        FlexBoxPane.setMargin(button, new Insets(5));
        FlexBoxPane.setFlexBasisPercent(button, 10f);
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                exit.navigate();
            }
        });
        flex.getChildren().add(button);

        InputStream input = this.getClass().getResourceAsStream("/logo.png");

        Image image = new Image(input);

        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(300);
        imageView.setFitWidth(300);

        mainVBox.getChildren().add(flex);
        Region spacer = new Region(), spacerSec = new Region();
        VBox.setVgrow(spacer, Priority.ALWAYS);
        VBox.setVgrow(spacerSec, Priority.ALWAYS);
        mainVBox.getChildren().add(spacer);
        mainVBox.getChildren().add(imageView);
        mainVBox.getChildren().add(spacerSec);
    }
}
