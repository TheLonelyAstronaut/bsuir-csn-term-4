package main.java.bsuir.csan.game.controllers;

import main.java.bsuir.csan.game.User;

public interface LoginCallback {
    void login(User _user);
}
