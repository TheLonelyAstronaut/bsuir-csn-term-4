package main.java.bsuir.csan.game;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.java.bsuir.csan.game.controllers.AppController;

public class Main extends Application {

    @Override
    public void start(Stage myStage) throws Exception{
        myStage.setTitle("Game 2048");

        DatabaseConnector.Connect();

        myStage.setOnCloseRequest(event -> {
            try{
                DatabaseConnector.Disconnect();
            }catch (Exception ex){

            }
            Platform.exit();
        });

        Game game = new Game();

        myStage.setMinHeight(400);
        myStage.setMinWidth(400);
        Scene scene = new Scene(new AppController(myStage),800,800);
        scene.getStylesheets().add("/styles.css");
        myStage.setScene(scene);

        myStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
